# Computer Store

A computer store where you can work, loan money and buy a computer

***

## Name
Computer Store

## Description
A computer store where you can work, loan money and buy a computer, all with the click from your mouse

## Installation
Have liveServer installed and open it in the HTML file

## Usage
Click around to see what you can do
INFO!! Clicking cancel instead of OK when taking a loan will cause you to take a loan of 0, simply click "Pay off loan" to fix

## Roadmap
Might fix image size to a standard size in a future update

## Authors and acknowledgment
Jessica Lindqvist

## Project status
Project is done for now, might make minor quality of life updates to make it look better in the future

